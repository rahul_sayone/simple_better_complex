from django.test import TestCase
from django.core.urlresolvers import reverse
from django.urls import resolve
from .views import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# Create your tests here.
class SignupTests(TestCase):
    def setUp(self):
        url=reverse('signup')
        self.response=self.client.get(url)

    def test_signup_status_code(self):
        url=reverse('signup')
        response=self.client.get(url)
        self.assertEquals(response.status_code,200)

    def test_signup_url_resolves_signup_views(self):
        view=resolve('/signup/')
        self.assertEquals(view.func,signup)

    def test_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_contains_form(self):
        form=self.response.context.get('form')
        self.assertIsInstance(form,UserCreationForm)


class SuccessfullSignUpTest(TestCase):
    def setUp(self):
        url=reverse('signup')
        data={"username":"sayone",
              "email":"again@again.com",
              "password1":"kakkanad",
              "password2":"kakkanad"
            }
        self.response=self.client.post(url,data)
        self.home_url=reverse('home')


    def test_redirection(self):
        self.assertRedirects(self.response,self.home_url)

    def test_user_creation(self):
        self.assertTrue(User.objects.exists())

    def test_user_is_authenticated(self):
        response=self.client.get(self.home_url)
        user=response.context.get('user')
        self.assertTrue(user.is_authenticated)