from django.test import TestCase
from django.core.urlresolvers import reverse
from django.urls import resolve
from ..views import home,board_topics,new_topics
from ..models import *
from django.contrib.auth.models import User
from ..forms import NewTopicForm
# Create your tests here.


class HomeTests(TestCase):

    def setUp(self):
        self.board=Board.objects.create(name='django', description='django board')
        url=reverse('home')
        self.response=self.client.get(url)

    def test_home_view_status_code(self):

        self.assertEquals(self.response.status_code,200)

    def test_home_url_resolves_home_view(self):
        view = resolve('/')
        self.assertEquals(view.func, home)

    def test_home_view_contains_link_to_topics_page(self):
        board_topics_url=reverse('board_topics',kwargs={'pk':self.board.pk})
        self.assertContains(self.response,'href="{0}"'.format(board_topics_url))


class BoardtopicsTests(TestCase):

    def setUp(self):
        Board.objects.create(name='Django',description='hello django test')

    def test_boardtopics_success_status_code_found(self):
        url=reverse('board_topics',kwargs={'pk':1})
        response=self.client.get(url)
        self.assertEquals(response.status_code,200)

    def test_boardtopics_status_code_notfound(self):
        url=reverse('board_topics',kwargs={'pk':99})
        response=self.client.get(url)
        self.assertEquals(response.status_code,404)

    def test_board_topics_url_resolves_board_topics_views(self):
        view=resolve('/boards/1/')
        self.assertEquals(view.func,board_topics)

    def test_board_topics_view_contains_link_back_to_homepage(self):
        board_topics_url=reverse('board_topics',kwargs={'pk':1})
        response=self.client.get(board_topics_url)
        homepage_url=reverse('home')
        self.assertContains(response,'href="{0}"'.format(homepage_url))


class NewTopicTests(TestCase):
    def setUp(self):
        Board.objects.create(name='Django',description='hello django test')

    def test_new_topic_success_status_code_found(self):
        url=reverse('new_topic',kwargs={'pk':1})
        response=self.client.get(url)
        self.assertEquals(response.status_code,200)

    def test_new_topic_status_code_not_found(self):
        url=reverse('new_topic',kwargs={'pk':99})
        response=self.client.get(url)
        self.assertEquals(response.status_code,404)

    def new_topic_url_resolves_new_topic_view(self):
        view=('boards/1/new')
        self.assertEquals(view.func,new_topics)

    def test_new_topic_views_contains_link_back_to_board_topics_view(self):
        new_topic_url=reverse('new_topic',kwargs={'pk':1})
        response=self.client.get(new_topic_url)
        board_page_url=reverse('board_topics',kwargs={'pk':1})
        self.assertContains(response,'href="{0}"'.format(board_page_url))


class BoardTopicsTest(TestCase):

    def setUp(self):
        Board.objects.create(name='Django',description='hello django test')

    def test_board_topics_contains_navigation_links(self):
        url=reverse('board_topics',kwargs={'pk':1})
        home_url=reverse('home')
        new_topic_url=reverse('new_topic',kwargs={'pk':1})
        response=self.client.get(url)
        self.assertContains(response,'href="{0}"'.format(home_url))
        self.assertContains(response,'href="{0}"'.format(new_topic_url))

class NeWTopicTests(TestCase):

    def setUp(self):
        User.objects.create(username='ajith',password='hello123')
        Board.objects.create(name='Django',description='hello django test')

    def test_csrf(self):
        url=reverse('new_topic',kwargs={'pk':1})
        response=self.client.get(url)
        self.assertContains(response,'csrfmiddlewaretoken')

    def test_new_topic_valid_post(self):
        url=reverse('new_topic',kwargs={'pk':1})
        data={'subject':'Test data', 'message':'Test message'
             }
        response=self.client.post(url,data)
        self.assertTrue(Topic.objects.exists())
        self.assertTrue(Post.objects.exists())

    def test_new_topic_post_data_empty_fields(self):
        url=reverse('new_topic',kwargs={'pk':1})
        data={}
        response=self.client.post(url,data)
        self.assertEquals(response.status_code,200)

    def test_new_topic_post_invalid_data(self):
        url=reverse('new_topic',kwargs={'pk':1})
        data={'subject':'','message':''}
        response=self.client.post(url,data)
        self.assertEquals(response.status_code,200)

    def test_contains_form(self):
        url=reverse('new_topic',kwargs={'pk':1})
        response=self.client.get(url)
        form=response.context.get('form')
        self.assertIsInstance(form,NewTopicForm)

    def test_new_topic_invalid_post_data(self):
        url=reverse('new_topic',kwargs={'pk':1})
        response=self.client.post(url,{})
        form=response.context.get('form')
        self.assertEquals(response.status_code,200)
        self.assertTrue(form.errors)
