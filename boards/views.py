from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse
from .models import *
from django.http import Http404
from django.contrib.auth.models import User
from .forms import NewTopicForm,Postform
from django.contrib.auth.decorators import login_required
# Create your views here.

def home(request):
    boards=Board.objects.all()
    return render(request,'home.html',{'boards':boards})

def board_topics(request,pk):
    board=get_object_or_404(Board,pk=pk)
    return render(request,'topics.html',{'board':board})

@login_required
def new_topics(request,pk):
    board=get_object_or_404(Board,pk=pk)
    user=request.user
    if request.method=='POST':
       form=NewTopicForm(request.POST)
       if form.is_valid():
        topic=form.save(commit=False)
        topic.board=board
        topic.starter=user
        topic.save()
        post = Post.objects.create(
            message=form.cleaned_data.get('message'),
            topic=topic,
            created_by=user
        )
        return redirect('topic_posts', pk=pk, topic_pk=topic.pk)
    else:
        form=NewTopicForm()
    return render(request,'new_topics.html',{'board':board,'form':form})


@login_required
def topic_posts(request,pk,topic_pk):
    topic=get_object_or_404(Topic,board__pk=pk,pk=topic_pk)
    return render(request,'topic_posts.html',{'topic':topic})

@login_required
def reply_topic(request, pk, topic_pk):
    topic = get_object_or_404(Topic, board__pk=pk, pk=topic_pk)
    if request.method == 'POST':
        form = Postform(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.topic = topic
            post.created_by = request.user
            post.save()
            return redirect('topic_posts', pk=pk, topic_pk=topic_pk)
    else:
        form = Postform()
    return render(request, 'reply_topic.html', {'topic': topic, 'form': form})
